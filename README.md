Early days .... Migrating an internal Red Hat project for importing documentation
from Publican and Asciidoc to Drupal as book nodes.

Currently Implemented
=====================
Drush:
- Command to list all books and provide comment counts
- Delete a single or multiple books

To come
=======
- Feed importer to import a publican / asciidoc book
- REST API support
- Administration panel
- Metatag support
- Landing Pages and TPL support
- Taxonomies for sorting / cataloguing books.