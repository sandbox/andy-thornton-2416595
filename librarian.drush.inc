<?php
/**
 * Drush commands for managing books
 */

/**
 * Implements books_drush_help().
 */
function librarian_drush_help($command) {
  switch ($command) {
    case 'drush:librarian':
      return dt('Tools for managing Drupal books');
    case 'meta:librarian:title':
      return dt("Librarian Book Tools");
    case 'meta:librarian:summary':
      return dt("Tools for managing drupal books");
  }
}

/**
 * Implements hook_drush_command().
 */
function librarian_drush_command() {
  $items['library-list'] = array(
    'callback' => 'librarian_drush_books',
    'description' => "Return an array for all current books",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'aliases' => array('liblist'),
  );
  $items['library-delete'] = array(
    'callback' => 'librarian_drush_delete',
    'description' => "Delete a book",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'aliases' => array('libdel'),
    'arguments' => array(
        'node' => 'Node ID of a page in the book you want to delete',
    )
  );
  $items['library-list-nodes'] = array(
    'callback' => 'redhat_docs_drush_list_nodes',
    'description' => "List all nodes in a book taking one nid as seed",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('liblnodes'),
    'arguments' => array(
      'nid' => 'Node ID from a page in a book',
    ),
  );
  $items['library-multi-delete'] = array(
    'callback' => 'librarian_drush_mdelete',
    'description' => "Delete multiple books",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'aliases' => array('libmdel'),
    'arguments' => array(
        'node' => 'Comma seperated list of nodes of books you want to delete, see "drush blist"',
   )
 );
    return $items;
}

/**
 * Command: List all nodes inside a book.
 *
 * @param string $nid
 *   Node ID for any internal book page
 *
 * @return array
 *   Array of book nodes.
 */
function _librarian_drush_list_nodes($nid) {
  $response = _librarian_book_nodes($nid);
  drush_print("Node passed = $nid");
  foreach($response as $nid) {
    $node = node_load($nid);
    $title = $node->title;
    $nodelist[] = array(
      $nid,
      $title,
    );
  }
  $header = array(dt('Node'), dt('Title'));
  array_unshift($nodelist, $header);
  drush_print_table($nodelist, TRUE);

}

/**
 * command: List Books
 */
function librarian_drush_books() {
  $books = book_get_books();
  $header = array(dt('Node'), dt('Comments'),dt('Title'));
  if (!empty($books)) {
    foreach ($books as $book) {
      $table[] = array($book['nid'], _librarian_comment_count($book['nid']), $book['link_title'] );
    }
  array_unshift($table,$header);
  drush_print_table($table,TRUE);
  } else {
    print "No books found\n";
  }
  return;
}

/**
 * command: Delete a book
 */
function librarian_drush_delete($node) {
  if (drush_confirm('Are you sure you want delete this book?')) {
    $response = _librarian_delete_book($node);
  } else {
    $response = "Delete cancelled\n";
  }
  return $response;
}

/**
 * command: Delete multiple books
 */
function librarian_drush_mdelete($nodes) {
  if (drush_confirm('Are you sure you want delete these books?')) {
    $books = explode(",", $nodes);
    foreach ($books as $book) {
      $tmp = _librarian_delete_book($book);
      print "Book $book deleted.\n";
      }
    } else {
      $response = "Delete cancelled\n";
    }
  return $response;
}