<?php

/**
 * @file
 * Views integration for the Docs module
 */

/**
 * Implements hook_views_data()
 * Notes from http://www.mydons.com/how-to-expose-custom-module-table-to-views-in-drupal/.
 *
 */
function librarian_views_data() {
  $data = array();
  $data['documentation']['table'] = array(
    'group' => 'Documentation',
    'base' => array(
      'field' => array('docid', 'id'),
      'title' => 'Documentation',
      'help' => 'Imported books from ccutil',
    ),
  );
  $data['documentation']['docid'] = array(
    'title' => 'Doc ID',
    'help' => 'Internal doc id',
    'field' => array(
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
      'help' => 'Sort Document\'s by doc id field.',
    ),
  );
  $data['documentation']['bid'] = array(
    'title' => 'Node',
    'help' => 'Book Node.',
    'field' => array(
      'click sortable' => TRUE,
    ),
  );
  $data['documentation']['xml'] = array(
    'title' => 'XML',
    'help' => 'XML Source of the book.',
    'field' => array(
      'click sortable' => FALSE,
    ),
  );
  $data['documentation']['title'] = array(
    'title' => 'Title',
    'help' => 'Book Title.',
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => 'Filter on title.',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['documentation']['version'] = array(
    'title' => 'Version',
    'help' => 'Product version.',
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => 'Filter on version.',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['documentation']['lang'] = array(
    'title' => 'Language',
    'help' => 'Book Language.',
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => 'Filter on language.',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['documentation']['commit'] = array(
    'title' => 'Commit',
    'help' => 'Commit value from which the book was built.',
    'field' => array(
      'click sortable' => FALSE,
    ),
  );
  return $data;
}
